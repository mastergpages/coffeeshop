1 How many times have you been to our establishment ?

    It was the first time

    Sometimes

    Frequently

2 How do you rate the reception upon arrival ?

    Not warm

    Ok

    Friendly

    Perfect

3 The decoration seemed appropriate to our business  ?

    I don't pay attention about decoration

    Not appropriate

    Was ok

    Yes it was really good

4 Does the atmosphere suit you, which element did not please you ?

    Music

    Light

    Smells

    Any

5 The variety of coffee offered in our menu was sufficient ?

    Poor, a lot of variety is missing

    Medium, some good variety is missing

    Full, very satisfied

6 Is the speed of service up to par ?

    Very slow, about 10 minutes

    Slow, about 5 minutes

    Fast, about 3 minutes

    Very fast, less than 3 minutes

7 Do you feel that the quantity of drinks is sufficient ?

    Not enough

    Was correct

    Too much

8 How to set the temperature of drinks ?

    Too Cold

    Cold

    Good

    Hot

    Too hot

9 The price seems right to you ?

  Cheap price

  Normal price

  High price

10 Do you use our reservation application ?

  No, never used

  Yes, some time sometime

  Yes, I use frequently

11 Were the chairs and benches comfortable ?

  It's not comfortable

  It's very comfortable

12 Are our business hours convenient for you ?

  No,

  Yes

13 Was the waiting period adequate, what time are you interested in ?

  6pm

  7pm

  7am

  It's good

14 Overall, how did you find your experience at our restaurant ?

  Bad, I am not convinced

  Ok, I like it

  Good, I'll come back

15 Do you have any other comments to help us improve our services ?

  input user
