import React, {useContext} from 'react'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import {FormControl, FormLabel, Radio, RadioGroup, TextField} from "@mui/material";
import FormControlLabel from '@mui/material/FormControlLabel'
import {AppContext} from '../Context'

export default function ThirdStep() {
  const { formValues, handleBack, handleNext, variant, margin } = useContext(AppContext)

  const handleSubmit = () => {
    let form = {}

    Object.keys(formValues).map((name) => {
      form = {
        ...form,
        [name]: formValues[name].value
      }
      return form
    })
    console.log(form)
    handleNext()
  }

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">Were the chairs and benches comfortable ?</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="It's not comfortable" />
              <FormControlLabel value="male" control={<Radio />} label="It's very comfortable" />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">Are our business hours convenient for you ?</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="No" />
              <FormControlLabel value="male" control={<Radio />} label="Yes" />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">Was the waiting period adequate, what time are you interested in ?</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="6pm" />
              <FormControlLabel value="male" control={<Radio />} label="7pm" />
              <FormControlLabel value="other" control={<Radio />} label="7am" />
              <FormControlLabel value="other" control={<Radio />} label="It's good" />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">Overall, how did you find your experience at our coffee shop ?</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="Bad, I am not convinced" />
              <FormControlLabel value="male" control={<Radio />} label="Ok, I like it" />
              <FormControlLabel value="other" control={<Radio />} label="Good, I'll come back" />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12}>
          <TextField
            variant={variant}
            margin={margin}
            fullWidth
            label='Do you have any other comments to help us improve our services ?'
            placeholder='Do you have any other comments to help us improve our services ?'
          />
        </Grid>
      </Grid>
      <Box height="1000px"/>

      <Box sx={{ display: 'flex', justifyContent: 'flex-end', mt: 3 }}>
        <Button onClick={handleBack} sx={{ mr: 1 }}>
          Back
        </Button>
        <Button variant='contained' color='success' onClick={handleSubmit}>
          Confirm & Continue
        </Button>
      </Box>
    </>
  )
}
