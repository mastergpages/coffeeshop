import React, {useContext} from 'react'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import {FormControl, FormLabel, Radio, RadioGroup} from "@mui/material";
import FormControlLabel from "@mui/material/FormControlLabel";
import {AppContext} from '../Context'

export default function FirstStep() {
  const { handleNext} = useContext(AppContext)

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">How many times have you been to our establishment ?</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="It was the first time" />
              <FormControlLabel value="male" control={<Radio />} label="Sometimes" />
              <FormControlLabel value="other" control={<Radio />} label="Frequently" />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">How do you rate the reception upon your arrival ?</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="Not warm" />
              <FormControlLabel value="male" control={<Radio />} label="Ok" />
              <FormControlLabel value="other" control={<Radio />} label="Friendly" />
              <FormControlLabel value="other" control={<Radio />} label="Perfect" />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">The decoration seemed appropriate to our business ?</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="I don't pay attention about decoration" />
              <FormControlLabel value="male" control={<Radio />} label="Not appropriate" />
              <FormControlLabel value="other" control={<Radio />} label="Was ok" />
              <FormControlLabel value="other" control={<Radio />} label="Yes it was really good" />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">Does the atmosphere suit you, which element did not please you ?</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="Music" />
              <FormControlLabel value="male" control={<Radio />} label="Light" />
              <FormControlLabel value="other" control={<Radio />} label="Smells" />
              <FormControlLabel value="other" control={<Radio />} label="Any" />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">Is the speed of service up to par ?</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="Very slow, about 10 minutes" />
              <FormControlLabel value="male" control={<Radio />} label="Slow, about 5 minutes" />
              <FormControlLabel value="other" control={<Radio />} label="Fast, about 3 minutes" />
              <FormControlLabel value="other" control={<Radio />} label="Very fast, less than 3 minutes" />
            </RadioGroup>
          </FormControl>
        </Grid>
      </Grid>

      <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button
          variant='contained'
          sx={{ mt: 3, ml: 1 }}
          color='primary'
          onClick={handleNext}
        >
          Next
        </Button>
      </Box>
    </>
  )
}
