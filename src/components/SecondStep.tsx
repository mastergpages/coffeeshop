import React, {useContext} from 'react'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import {FormControl, FormLabel, Radio, RadioGroup} from "@mui/material";
import FormControlLabel from '@mui/material/FormControlLabel'
import {AppContext} from '../Context'

export default function SecondStep() {
  const {handleBack, handleNext} = useContext(AppContext)

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">The variety of coffee offered in our menu was sufficient  ?</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="Poor, a lot of variety is missing" />
              <FormControlLabel value="male" control={<Radio />} label="Medium, some good variety is missing" />
              <FormControlLabel value="other" control={<Radio />} label="Full, very satisfied" />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">Do you feel that the quantity of drinks is sufficient ?</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="Not enough" />
              <FormControlLabel value="male" control={<Radio />} label="Was correct" />
              <FormControlLabel value="other" control={<Radio />} label="Too much" />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">How to set the temperature of drinks ?</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="Too Cold" />
              <FormControlLabel value="male" control={<Radio />} label="Cold" />
              <FormControlLabel value="other" control={<Radio />} label="Good" />
              <FormControlLabel value="other" control={<Radio />} label="Hot" />
              <FormControlLabel value="other" control={<Radio />} label="Too hot" />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">The price seems right to you ?</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="Cheap price" />
              <FormControlLabel value="male" control={<Radio />} label="Normal price" />
              <FormControlLabel value="other" control={<Radio />} label="High price" />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={12}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">Do you use our reservation application ?</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="No, never used" />
              <FormControlLabel value="male" control={<Radio />} label="Yes, sometime" />
              <FormControlLabel value="other" control={<Radio />} label="Yes, I use frequently" />
            </RadioGroup>
          </FormControl>
        </Grid>
      </Grid>

      <Box sx={{ display: 'flex', justifyContent: 'flex-end', mt: 3 }}>
        <Button onClick={handleBack} sx={{ mr: 1 }}>
          Back
        </Button>
        <Button variant='contained' color='primary' onClick={handleNext}>
          Next
        </Button>
      </Box>
    </>
  )
}
