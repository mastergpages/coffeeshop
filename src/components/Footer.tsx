import React, {memo} from 'react'
import Typography from '@mui/material/Typography'

function Footer() {
  return (
    <Typography variant='body2' color='textSecondary' align='center'>
      {'The Coffee Shop '}
    </Typography>
  )
}

export default memo(Footer)
