import React, {memo} from 'react'
import AppBar from '@mui/material/AppBar'
import Box from "@mui/material/Box";

function Header() {
  return (
    <AppBar sx={{ bgcolor: "white" }} position='static'>
        <Box margin="auto">
         <img height="90px" src="https://cdn.discordapp.com/attachments/525754788747411471/1049120780585283675/logo.png" alt=""/>
        </Box>
    </AppBar>
  )
}

export default memo(Header)
