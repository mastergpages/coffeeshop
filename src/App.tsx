import React from 'react'
import {createTheme, ThemeProvider} from '@mui/material/styles'
import CssBaseline from '@mui/material/CssBaseline'
import Container from '@mui/material/Container'
import Paper from '@mui/material/Paper'
import {Grid} from '@mui/material'
import StepForm from './components/StepForm'
import Header from './components/Header'
import Footer from './components/Footer'
import './styles.css';

const theme = createTheme()

export default function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Header />
      <Container component='main' sx={{ mb: 4 }}>
        <Paper variant='outlined' sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}>
          <Grid direction='row' container spacing={1}>
            <Grid container item sm={6}>
              <StepForm />
            </Grid>
            <Grid container item sm={6}>
                <img width="540" alt="" src="https://cdn.discordapp.com/attachments/525754788747411471/1049120977155526768/f91375_072ae68cdded4804830821753b33745c_mv2.webp" />
            </Grid>
          </Grid>
        </Paper>
        <Footer />
      </Container>
    </ThemeProvider>
  )
}
